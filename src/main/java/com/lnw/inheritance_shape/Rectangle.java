package com.lnw.inheritance_shape;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Maintory_
 */
public class Rectangle extends Shape {
    public Rectangle(String Shape){
        super(Shape);
        System.out.println("Rectangle Created");
    }
    @Override
    public void calArea(double val1,double val2){
        area = val1*val2;
        System.out.println("Area = " +area);
    }
}
