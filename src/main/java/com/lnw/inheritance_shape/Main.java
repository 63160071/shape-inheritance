/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.inheritance_shape;

/**
 *
 * @author Maintory_
 */
public class Main {
    public static void main(String[] args) {
        Shape shape = new Shape("shape");
        Rectangle rectangle = new Rectangle("Rectangle");
        rectangle.calArea(1,4);
        Square square = new Square("Square");
        square.calArea(3,3);
        Triangle triangle = new Triangle("Triangle");
        triangle.calArea(3,3);
        Circle circle = new Circle("Circle");
        circle.calArea(3,3);

    }
}
